# docker-compose-repo
This repo contains docker-compose files for all kind of applications.

## How to use
Every folder contains a .env.template file. This file contains some environment variables which can be set to personal needs. To do this, copy this file and rename it to .env.

When all variables are set in the .env file, execute 'run.bash' to start an application with docker-compose. 