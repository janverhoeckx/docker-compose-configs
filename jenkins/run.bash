#!/bin/bash

# 1. Check if .env file exists
if [ -e .env ]; then
    source .env
else 
    echo "Please set up your .env file before starting your enviornment."
    exit 1
fi

# 2. build custom docker image
docker build -t jenkins .

# 3. start container
docker-compose up -d

exit 0